// alert("hello")

// let posts = [];
// let count = 1;

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
.then(data => {
	showPosts(data)
	console.log(data)
})
let addForm = document.querySelector('#form-add-post')

addForm.addEventListener("submit", (e) =>{

	// console.log(e)
	// preventDefault() stops the form from reloading the page and resetting our JS code
	e.preventDefault()

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})

	count++;

	// call the showPosts 
	showPosts(posts);
	console.log(posts);
})

const showPosts = (posts) => {
	let postEntries = '';

	// loop through each post in our posts array
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	})

	// insert the postEntries HTML code into the empty div in our HTML
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}



const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	//populate the edit form fields
	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}


document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault()

	// user a for loop to match the id of the post to be edited with the post inside of our posts array
	for(let i = 0; i < posts.length; i++){
		if(document.querySelector('#txt-edit-id').value === posts[i].id.toString()){

			// reassign the title and body of the post in the array to the new title and body
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			// call showPosts again to update the output
			showPosts(posts)

			// user break to end the loop
			break;
		}
	}
})

// ACTIVITY
// Delete Post
const deletePost = (id) => {

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}` , {
		method: 'DELETE'
	})

	alert("Post Succesfully Deleted");
	document.querySelector(`#post-${id}`).remove();

};

